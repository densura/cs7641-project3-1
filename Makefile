NOW := $(shell date "+%Y-%m-%d_%H:%M:%S")

kmeans_segmentation:
	python3 kmeans_segmentation.py > output/kmeans_segmentation_$(NOW).out

kmeans_adult:
	python3 kmeans_adult.py > output/kmeans_adult_$(NOW).out