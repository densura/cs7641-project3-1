import csv
from datetime import datetime

from sklearn import mixture

import curve
from adult_dataset import load_train_test_data

N_COMPONENTS_RANGE = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]


def pairplot(X, y_labels, n_components, covariance_type):
    gmm = mixture.GaussianMixture(
        random_state=0, n_components=n_components, covariance_type=covariance_type
    )
    gmm.fit(X)
    y_predict = gmm.predict(X)
    df = X
    df["CLUSTER-LABEL"] = y_predict
    curve.gt_pairplot(
        df,
        hue="CLUSTER-LABEL",
        file_name="plots/em-adult-pairplot-all",
        n_colors=n_components,
    )
    curve.gt_pairplot(
        df,
        hue="CLUSTER-LABEL",
        file_name="plots/em-adult-pairplot-select",
        n_colors=n_components,
        var_list=[
            "capital-gain",
            "age",
            "marital-status",
        ],
    )

    from sklearn import metrics
    rand_score = metrics.rand_score(y_predict, y_labels['income'])
    rand_score_data_file_name = "plots/{}_{}.csv".format(
        "adult-em-rand_score", datetime.now()
    )
    with open(rand_score_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerow(["rand_score", rand_score])


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data()
    curve.plot_gmm_bic(X, N_COMPONENTS_RANGE, "adult")
    pairplot(X, y, n_components=7, covariance_type="diag")


if __name__ == "__main__":
    main()
