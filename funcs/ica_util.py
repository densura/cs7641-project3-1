import csv

import scipy
from sklearn.decomposition import PCA, FastICA
from datetime import datetime
import matplotlib.pyplot as plt


def run_pca(X, n_components, file_prefix):
    pca = PCA(random_state=0, n_components=n_components)
    pca.fit(X)

    distribution_eigenvalues_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "pca-distribution-eigenvalues", datetime.now()
    )
    distribution_eigenvalues_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "pca-distribution-eigenvalues", datetime.now()
    )

    x = range(0, len(pca.explained_variance_ratio_))
    plt.cla()
    plt.bar(x, pca.explained_variance_ratio_, width=0.4)
    plt.xticks(x)
    plt.title("Distribution of Eigenvalues")
    plt.xlabel("Principal Component")
    plt.ylabel("Explained Variance Ratio")
    plt.savefig(distribution_eigenvalues_plot_file_name, format="png")

    with open(distribution_eigenvalues_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerows([i for i in zip(x, pca.explained_variance_ratio_)])


def ica_choose_components(X, n_components_list, file_prefix):
    import numpy as np

    errors = []
    kurtosis = []
    for n_components in n_components_list:
        ica = FastICA(random_state=0, n_components=n_components)
        ica.fit(X)
        Xt = ica.transform(X)
        Xp = ica.inverse_transform(Xt)
        error = np.sum((X - Xp) ** 2, axis=1).mean()
        k = scipy.stats.kurtosis(ica.transform(X))
        kurtosis.append(np.mean(np.abs(k)))
        errors.append(error)

    import matplotlib.pyplot as plt

    recons_error_kurtosis_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "ica-reconstruction-error-kurtosis", datetime.now()
    )
    recons_error_kurtosis_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "ica-reconstruction-error-kurtosis", datetime.now()
    )

    # Reconstruction errors
    plt.figure(0)
    plt.plot(n_components_list, errors, label="Reconstruction Error")
    plt.plot(n_components_list, kurtosis, label="Mean Abs. Kurtosis")
    plt.grid()
    plt.xticks(n_components_list)
    plt.title("Reconstruction Error & Kurtosis (ICA)")
    plt.xlabel("Number of Components")
    plt.legend(loc="best")
    plt.savefig(recons_error_kurtosis_plot_file_name, format="png")
    with open(recons_error_kurtosis_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerow(["n_components", "reconstruction_error", "kurtosis"])
        writer.writerows(zip(n_components_list, errors, kurtosis))
