import csv

from sklearn.decomposition import PCA
from datetime import datetime
import matplotlib.pyplot as plt


def run_pca(X, n_components, file_prefix):
    import numpy as np

    pca = PCA(random_state=0, n_components=n_components)
    pca.fit(X)

    distribution_eigenvalues_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "pca-distribution-eigenvalues", datetime.now()
    )
    distribution_eigenvalues_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "pca-distribution-eigenvalues", datetime.now()
    )
    print(pca.components_)
    x = range(0, len(pca.explained_variance_ratio_))
    plt.cla()
    plt.bar(x, pca.explained_variance_ratio_, width=0.4)
    plt.xticks(x)
    plt.title("Distribution of Eigenvalues")
    plt.xlabel("Principal Component")
    plt.ylabel("Explained Variance Ratio")
    plt.savefig(distribution_eigenvalues_plot_file_name, format="png")

    print(np.cumsum(pca.explained_variance_ratio_))

    with open(distribution_eigenvalues_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerows([i for i in zip(x, pca.explained_variance_ratio_)])


def pca_choose_components(X, n_components_list, file_prefix):
    import numpy as np

    errors = []
    for n_components in n_components_list:
        pca = PCA(random_state=0, n_components=n_components)
        pca.fit(X)
        Xt = pca.transform(X)
        Xp = pca.inverse_transform(Xt)
        error = np.sum((X - Xp) ** 2, axis=1).mean()
        errors.append(error)

    import matplotlib.pyplot as plt

    recons_error_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "pca-reconstruction-error", datetime.now()
    )
    explained_variance_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "pca-explained-variance", datetime.now()
    )
    recons_error_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "pca-reconstruction-error", datetime.now()
    )
    explained_variance_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "pca-explained-variance", datetime.now()
    )

    # Reconstruction errors
    plt.figure(0)
    plt.plot(n_components_list, errors)
    plt.grid()
    plt.xticks(n_components_list)
    plt.title("Reconstruction Error (PCA)")
    plt.xlabel("Number of Components")
    plt.ylabel("Reconstruction Error")
    plt.savefig(recons_error_plot_file_name, format="png")
    with open(recons_error_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerows([[error] for error in errors])

    # Explained variance
    pca = PCA(random_state=0, n_components=list(n_components_list)[-1]).fit(X)
    cumsum = np.cumsum(pca.explained_variance_ratio_)
    plt.figure(1)
    plt.grid()
    plt.plot(cumsum)
    plt.xlabel("Number of components")
    plt.ylabel("Cumulative explained variance")
    plt.savefig(explained_variance_plot_file_name, format="png")

    with open(explained_variance_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerows([[x] for x in cumsum])
