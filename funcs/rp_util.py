import csv

from datetime import datetime


def rp_calculate_variances(X, n_components_list, num_times, file_prefix):
    import numpy as np
    from sklearn import random_projection
    import matplotlib.pyplot as plt

    variances = []
    for n_components in n_components_list:
        errors = []
        for _ in range(0, num_times):
            grp = random_projection.GaussianRandomProjection(n_components=n_components)
            errors.append(rp_calculate_error(X, transformer=grp))
        variances.append(np.var(errors))

    rp_variances_plot_name = "plots/{}_{}_{}.png".format(
        file_prefix, "rp-error-variance", datetime.now()
    )
    rp_variances_data_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "rp-error-variance", datetime.now()
    )

    plt.cla()
    plt.plot(n_components_list, variances)
    plt.grid()
    plt.xticks(n_components_list)
    plt.title("Error Variance Across Runs")
    plt.xlabel("Number of Components")
    plt.ylabel("Variance")
    plt.savefig(rp_variances_plot_name, format="png")
    with open(rp_variances_data_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerows([[error] for error in errors])


def rp_calculate_error(X, transformer):
    import numpy as np

    transformer.fit(X)
    Xt = transformer.transform(X)
    Xp = Xt.dot(transformer.components_) + np.mean(X.values, axis=0)
    re = np.mean((X - Xp) ** 2)  # Mean Squared Error
    return re


def rp_choose_components(X, n_components_list, file_prefix):
    import numpy as np
    from sklearn import random_projection

    """
    #RP
    A=X
    transformer = GaussianRandomProjection(n_components=4)
    X = transformer.fit_transform(X)
    W=transformer.components_
    
    #numpy pinv
    W_pinv=LA.pinv(W).transpose()
    B = np.dot(X, W_pinv)
    
    #calc. dist. from original points
    dist = np.linalg.norm(A-B)
    """

    errors = []
    for n_components in n_components_list:
        grp = random_projection.GaussianRandomProjection(
            random_state=0, n_components=n_components
        )
        grp.fit(X)
        Xt = grp.transform(X)
        Xp = Xt.dot(grp.components_) + np.mean(X.values, axis=0)
        re = np.mean((X - Xp) ** 2)  # Mean Squared Error
        errors.append(np.mean(re.values))

    import matplotlib.pyplot as plt

    recons_error_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "rp-reconstruction-error", datetime.now()
    )
    recons_error_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "rp-reconstruction-error", datetime.now()
    )

    # Reconstruction errors
    plt.figure(0)
    plt.plot(n_components_list, errors)
    plt.grid()
    plt.xticks(n_components_list)
    plt.title("Reconstruction Errors (RP)")
    plt.xlabel("Number of Components")
    plt.ylabel("Error")
    plt.savefig(recons_error_plot_file_name, format="png")
    with open(recons_error_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerows([[error] for error in errors])
