from sklearn.decomposition import TruncatedSVD
import csv
from datetime import datetime


def tsvd_choose_components(X, n_components_list, file_prefix):
    import numpy as np

    errors = []
    for n_components in n_components_list:
        tsvd = TruncatedSVD(random_state=0, n_components=n_components)
        tsvd.fit(X)
        Xt = tsvd.transform(X)
        Xp = tsvd.inverse_transform(Xt)
        error = np.sum((X - Xp) ** 2, axis=1).mean()
        errors.append(error)

    import matplotlib.pyplot as plt

    recons_error_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "tsvd-reconstruction-error", datetime.now()
    )
    explained_variance_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "tsvd-explained-variance", datetime.now()
    )
    recons_error_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "tsvd-reconstruction-error", datetime.now()
    )
    explained_variance_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "tsvd-explained-variance", datetime.now()
    )

    # Reconstruction errors
    plt.figure(0)
    plt.plot(n_components_list, errors)
    plt.grid()
    plt.xticks(n_components_list)
    plt.title("Reconstruction Error (Truncated SVD)")
    plt.xlabel("Number of Components")
    plt.ylabel("Reconstruction Error")
    plt.savefig(recons_error_plot_file_name, format="png")
    with open(recons_error_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerows([[error] for error in errors])

    # Explained variance
    tsvd = TruncatedSVD(random_state=0, n_components=list(n_components_list)[-1]).fit(X)
    cumsum = np.cumsum(tsvd.explained_variance_ratio_)
    plt.figure(1)
    plt.grid()
    plt.plot(cumsum)
    plt.xlabel("Number of components")
    plt.ylabel("Cumulative explained variance")
    plt.savefig(explained_variance_plot_file_name, format="png")

    with open(explained_variance_data_file_name, "w", newline="") as output:
        writer = csv.writer(output, delimiter=",")
        writer.writerows([[x] for x in cumsum])
