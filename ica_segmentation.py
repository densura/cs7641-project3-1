from funcs.ica_util import ica_choose_components
from segmentation_dataset import load_train_test_data

N_COMPONENTS_LIST = range(2, 19)


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)
    ica_choose_components(X, list(N_COMPONENTS_LIST), "segmentation")


if __name__ == "__main__":
    main()
