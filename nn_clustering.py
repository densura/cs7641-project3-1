import numpy as np
import pandas as pd
from sklearn import mixture
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler, OneHotEncoder

from curve import show_learning_curve
from segmentation_dataset import load_train_test_data
from util import calculate_f1_score


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)

    kmeans_n_clusters = 7
    em_n_clusters = 9

    pca_n_components = 12
    pca = PCA(random_state=0, n_components=pca_n_components)
    pca.fit(X_train)

    X_train_t = pca.transform(X_train)
    X_test_t = pca.transform(X_test)

    standard_scaler = StandardScaler()
    one_hot_encoder = OneHotEncoder()

    kmeans_train = KMeans(n_clusters=kmeans_n_clusters, random_state=0)
    kmeans_train.fit(X_train_t)
    kmeans_clusters_train = pd.DataFrame(one_hot_encoder.fit_transform(pd.DataFrame(kmeans_train.predict(X_train_t))).toarray())
    kmeans_clusters_test = pd.DataFrame(one_hot_encoder.fit_transform(pd.DataFrame(kmeans_train.predict(X_test_t))).toarray())

    kmeans_distance_train = pd.DataFrame(standard_scaler.fit_transform((kmeans_train.transform(X_train_t) ** 2))).sum(axis=1)
    kmeans_distance_test = pd.DataFrame(standard_scaler.fit_transform((kmeans_train.transform(X_test_t) ** 2))).sum(axis=1)

    kmeans_X_train = pd.concat(
        [
            pd.DataFrame(X_train_t),
            pd.DataFrame(
                kmeans_clusters_train
            ).reset_index(drop=True),
            pd.DataFrame(
                kmeans_distance_train
            ).reset_index(drop=True),
        ],
        axis=1,
    ).reset_index(drop=True)
    kmeans_X_test = pd.concat(
        [
            pd.DataFrame(X_test_t),
            pd.DataFrame(
                kmeans_clusters_test
            ).reset_index(drop=True),
            pd.DataFrame(
                kmeans_distance_test
            ).reset_index(drop=True),
        ],
        axis=1,
    ).reset_index(drop=True)

    em_train = mixture.GaussianMixture(
        random_state=0, n_components=em_n_clusters, covariance_type="full"
    )
    em_train.fit(X_train_t)
    em_clusters_train = em_train.predict(X_train_t)
    em_clusters_test = em_train.predict(X_test_t)

    em_X_train = pd.concat(
        [
            pd.DataFrame(X_train_t),
            pd.DataFrame(
                em_clusters_train,
                columns=["cluster"]
            ).reset_index(drop=True),
        ],
        axis=1,
    )
    em_X_test = pd.concat(
        [
            pd.DataFrame(X_test_t),
            pd.DataFrame(
                em_clusters_test,
                columns=["cluster"]
            ).reset_index(drop=True),
        ],
        axis=1,
    )

    kmeans_mlp = MLPClassifier(random_state=0)
    kmeans_mlp.fit(kmeans_X_train, y_train.values.ravel())

    em_mlp = MLPClassifier(random_state=0)
    em_mlp.fit(em_X_train, y_train.values.ravel())

    show_learning_curve(
        MLPClassifier(random_state=0),
        kmeans_X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "Neural Network Learning Curve (KMeans)",
        ylabel="f1_macro_score",
        file_prefix="segmentation_kmeans",
        scoring="f1_macro",
    )

    print("\n== Base Training Score and Confusion Matrix (KMeans) ==")
    calculate_f1_score(kmeans_mlp, kmeans_X_train, y_train.values.ravel(), average="macro")
    print("\n== Base Test Score and Confusion Matrix (KMeans) ==")
    calculate_f1_score(kmeans_mlp, kmeans_X_test, y_test.values.ravel(), average="macro")

    show_learning_curve(
        MLPClassifier(random_state=0),
        em_X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "Neural Network Learning Curve (EM)",
        ylabel="f1_macro_score",
        file_prefix="segmentation_em",
        scoring="f1_macro",
    )

    print("\n== Base Training Score and Confusion Matrix (EM) ==")
    calculate_f1_score(em_mlp, em_X_train, y_train.values.ravel(), average="macro")
    print("\n== Base Test Score and Confusion Matrix (EM) ==")
    calculate_f1_score(em_mlp, em_X_test, y_test.values.ravel(), average="macro")


if __name__ == "__main__":
    main()
