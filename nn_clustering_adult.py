import numpy as np
import pandas as pd
from sklearn import mixture
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.neural_network import MLPClassifier

from curve import show_learning_curve
from adult_dataset import load_train_test_data


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)

    kmeans_n_clusters = 5
    em_n_clusters = 12

    pca_n_components = 8
    pca = PCA(random_state=0, n_components=pca_n_components)
    pca.fit(X_train)

    X_train_t = pca.transform(X_train)


    kmeans = KMeans(n_clusters=kmeans_n_clusters, random_state=0)
    kmeans.fit(X_train_t)
    kmeans_clusters = kmeans.predict(X_train_t)

    # centers = pd.DataFrame(kmeans.cluster_centers_[kmeans_clusters], columns=X_train_t.columns)
    # kmeans_center_distance = np.linalg.norm(centers, X_train_t)

    kmeans_X_train = pd.concat(
        [
            X_train.reset_index(drop=True),
            pd.DataFrame(
                kmeans_clusters,
                columns=["cluster"]
            ).reset_index(drop=True),
        ],
        axis=1,
    )

    gmm = mixture.GaussianMixture(
        random_state=0, n_components=em_n_clusters, covariance_type="full"
    )
    gmm.fit(X_train_t)
    gmm_clusters = gmm.predict(X_train_t)
    gmm_X_train = pd.concat(
        [
            X_train.reset_index(drop=True),
            pd.DataFrame(
                gmm_clusters,
                columns=["cluster"]
            ).reset_index(drop=True),
        ],
        axis=1,
    )

    MAX_ITER = 2000

    best_params = {'activation': 'relu', 'hidden_layer_sizes': (75,75), 'learning_rate_init': 0.001, 'solver': 'sgd', 'max_iter': MAX_ITER}
    kmeans_mlp = MLPClassifier(**best_params)
    kmeans_mlp.fit(kmeans_X_train, y_train.values.ravel())

    gmm_mlp = MLPClassifier(**best_params)
    gmm_mlp.fit(gmm_X_train, y_train.values.ravel())

    show_learning_curve(
        MLPClassifier(random_state=0),
        kmeans_X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "Neural Network Learning Curve (KMeans)",
        ylabel="f1_macro_score",
        file_prefix="adult_kmeans",
        scoring="f1_macro",
    )

    show_learning_curve(
        MLPClassifier(random_state=0),
        kmeans_X_train,
        y_train.values.ravel(),
        np.linspace(0.1, 1.0, 10),
        "Neural Network Learning Curve (EM)",
        ylabel="f1_macro_score",
        file_prefix="adult_em",
        scoring="f1_macro",
    )


if __name__ == "__main__":
    main()
