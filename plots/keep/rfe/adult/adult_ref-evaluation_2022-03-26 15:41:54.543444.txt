N_FEATURES=1
['relationship']
[(1, 'relationship'), (2, 'capital-gain'), (3, 'education-num'), (4, 'age'), (5, 'hours-per-week'), (6, 'occupation'), (7, 'workclass'), (8, 'capital-loss'), (9, 'native-country'), (10, 'race'), (11, 'education'), (12, 'marital-status'), (13, 'sex')]

N_FEATURES=2
['capital-gain', 'relationship']
[(1, 'capital-gain'), (1, 'relationship'), (2, 'education-num'), (3, 'age'), (4, 'hours-per-week'), (5, 'occupation'), (6, 'workclass'), (7, 'capital-loss'), (8, 'native-country'), (9, 'race'), (10, 'education'), (11, 'marital-status'), (12, 'sex')]

N_FEATURES=3
['education-num', 'capital-gain', 'relationship']
[(1, 'education-num'), (1, 'capital-gain'), (1, 'relationship'), (2, 'age'), (3, 'hours-per-week'), (4, 'occupation'), (5, 'workclass'), (6, 'capital-loss'), (7, 'native-country'), (8, 'race'), (9, 'education'), (10, 'marital-status'), (11, 'sex')]

N_FEATURES=4
['age', 'education-num', 'capital-gain', 'relationship']
[(1, 'age'), (1, 'education-num'), (1, 'capital-gain'), (1, 'relationship'), (2, 'hours-per-week'), (3, 'occupation'), (4, 'workclass'), (5, 'capital-loss'), (6, 'native-country'), (7, 'race'), (8, 'education'), (9, 'marital-status'), (10, 'sex')]

N_FEATURES=5
['age', 'education-num', 'capital-gain', 'hours-per-week', 'relationship']
[(1, 'age'), (1, 'education-num'), (1, 'capital-gain'), (1, 'hours-per-week'), (1, 'relationship'), (2, 'occupation'), (3, 'workclass'), (4, 'capital-loss'), (5, 'native-country'), (6, 'race'), (7, 'education'), (8, 'marital-status'), (9, 'sex')]

N_FEATURES=6
['age', 'education-num', 'capital-gain', 'hours-per-week', 'occupation', 'relationship']
[(1, 'age'), (1, 'education-num'), (1, 'capital-gain'), (1, 'hours-per-week'), (1, 'occupation'), (1, 'relationship'), (2, 'workclass'), (3, 'capital-loss'), (4, 'native-country'), (5, 'race'), (6, 'education'), (7, 'marital-status'), (8, 'sex')]

N_FEATURES=7
['age', 'education-num', 'capital-gain', 'hours-per-week', 'workclass', 'occupation', 'relationship']
[(1, 'age'), (1, 'education-num'), (1, 'capital-gain'), (1, 'hours-per-week'), (1, 'workclass'), (1, 'occupation'), (1, 'relationship'), (2, 'capital-loss'), (3, 'native-country'), (4, 'race'), (5, 'education'), (6, 'marital-status'), (7, 'sex')]

N_FEATURES=8
['age', 'education-num', 'capital-gain', 'capital-loss', 'hours-per-week', 'workclass', 'occupation', 'relationship']
[(1, 'age'), (1, 'education-num'), (1, 'capital-gain'), (1, 'capital-loss'), (1, 'hours-per-week'), (1, 'workclass'), (1, 'occupation'), (1, 'relationship'), (2, 'native-country'), (3, 'race'), (4, 'education'), (5, 'marital-status'), (6, 'sex')]

N_FEATURES=9
['age', 'education-num', 'capital-gain', 'capital-loss', 'hours-per-week', 'workclass', 'occupation', 'relationship', 'native-country']
[(1, 'age'), (1, 'education-num'), (1, 'capital-gain'), (1, 'capital-loss'), (1, 'hours-per-week'), (1, 'workclass'), (1, 'occupation'), (1, 'relationship'), (1, 'native-country'), (2, 'race'), (3, 'education'), (4, 'marital-status'), (5, 'sex')]

N_FEATURES=10
['age', 'education-num', 'capital-gain', 'capital-loss', 'hours-per-week', 'workclass', 'occupation', 'relationship', 'race', 'native-country']
[(1, 'age'), (1, 'education-num'), (1, 'capital-gain'), (1, 'capital-loss'), (1, 'hours-per-week'), (1, 'workclass'), (1, 'occupation'), (1, 'relationship'), (1, 'race'), (1, 'native-country'), (2, 'education'), (3, 'marital-status'), (4, 'sex')]

N_FEATURES=11
['age', 'education-num', 'capital-gain', 'capital-loss', 'hours-per-week', 'workclass', 'occupation', 'relationship', 'race', 'native-country', 'education']
[(1, 'age'), (1, 'education-num'), (1, 'capital-gain'), (1, 'capital-loss'), (1, 'hours-per-week'), (1, 'workclass'), (1, 'occupation'), (1, 'relationship'), (1, 'race'), (1, 'native-country'), (1, 'education'), (2, 'marital-status'), (3, 'sex')]

N_FEATURES=12
['age', 'education-num', 'capital-gain', 'capital-loss', 'hours-per-week', 'workclass', 'marital-status', 'occupation', 'relationship', 'race', 'native-country', 'education']
[(1, 'age'), (1, 'education-num'), (1, 'capital-gain'), (1, 'capital-loss'), (1, 'hours-per-week'), (1, 'workclass'), (1, 'marital-status'), (1, 'occupation'), (1, 'relationship'), (1, 'race'), (1, 'native-country'), (1, 'education'), (2, 'sex')]

N_FEATURES=13
['age', 'education-num', 'capital-gain', 'capital-loss', 'hours-per-week', 'workclass', 'marital-status', 'occupation', 'relationship', 'race', 'sex', 'native-country', 'education']
[(1, 'age'), (1, 'education-num'), (1, 'capital-gain'), (1, 'capital-loss'), (1, 'hours-per-week'), (1, 'workclass'), (1, 'marital-status'), (1, 'occupation'), (1, 'relationship'), (1, 'race'), (1, 'sex'), (1, 'native-country'), (1, 'education')]

