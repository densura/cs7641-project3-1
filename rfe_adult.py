from sklearn.tree import DecisionTreeClassifier

import rfe_util
from adult_dataset import load_train_test_data


def main():
    X_train, X_test, y_train, y_test, X, y, df = load_train_test_data(small=False)
    rfe_util.evaluate_rfe(X_train, y_train, estimator=DecisionTreeClassifier(random_state=0),
                          n_features_list=range(1, 14),
                          file_prefix="adult")
    rfe_util.run_ref(X_train, y_train, estimator=DecisionTreeClassifier(random_state=0), file_prefix="adult",
                     n_features=11)


if __name__ == "__main__":
    main()
