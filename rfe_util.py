from datetime import datetime

from sklearn.feature_selection import RFE


def run_ref(X, y, estimator, n_features, file_prefix):
    import numpy as np
    import csv

    rfe = RFE(estimator, n_features_to_select=n_features, step=1)
    rfe.fit(X, y)

    txt_file_name = "plots/{}_{}_{}.txt".format(
        file_prefix, "rfe-output", datetime.now()
    )
    rankings = []
    for idx in range(len(rfe.ranking_)):
        rank = rfe.ranking_[idx]
        rankings.append((rank, X.columns[idx]))

    with open(txt_file_name, "w", newline="") as f:
        f.write(f"N_FEATURES={n_features}\n")
        f.write(str(list(X.columns[rfe.support_])) + "\n")
        f.write(str(sorted(rankings, key=lambda x: x[0])) + "\n\n")


def evaluate_rfe(X, y, estimator, n_features_list, file_prefix):
    txt_file_name = "plots/{}_{}_{}.txt".format(
        file_prefix, "ref-evaluation", datetime.now()
    )

    reconstruction_errors_data_file_name = "plots/{}_{}_{}.csv".format(
        file_prefix, "ref-reconstruction-errors", datetime.now()
    )
    reconstruction_errors_plot_file_name = "plots/{}_{}_{}.png".format(
        file_prefix, "ref-reconstruction-errors", datetime.now()
    )

    import numpy as np
    import csv

    errors = []
    with open(txt_file_name, "w", newline="") as f:
        for n_features in n_features_list:
            rfe = RFE(estimator, n_features_to_select=n_features, step=1)
            rfe.fit(X, y)

            f.write(f"N_FEATURES={n_features}\n")
            f.write(str(list(X.columns[rfe.support_])) + "\n")
            rankings = []
            for idx in range(len(rfe.ranking_)):
                rank = rfe.ranking_[idx]
                rankings.append((rank, X.columns[idx]))

            f.write(str(sorted(rankings, key=lambda x: x[0])) + "\n\n")

            Xt = rfe.transform(X)
            Xp = rfe.inverse_transform(Xt)
            error = np.sum((X - Xp) ** 2, axis=1).mean()
            errors.append(error)

    with open(reconstruction_errors_data_file_name, "w", newline="") as f:
        writer = csv.writer(f, delimiter=",")
        writer.writerows([[err] for err in errors])

    import matplotlib.pyplot as plt

    plt.plot(n_features_list, errors)
    plt.xticks(n_features_list)
    plt.xlabel("Number of features")
    plt.ylabel("Reconstruction errors")
    plt.grid()
    plt.savefig(reconstruction_errors_plot_file_name, format="png")
