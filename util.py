import itertools
from datetime import datetime

import scipy
from sklearn.decomposition import PCA, FastICA
from sklearn.model_selection import GridSearchCV
from curve import plot_learning_curve
from sklearn.metrics import confusion_matrix, f1_score, mutual_info_score
import numpy as np
import pandas as pd
import time

import matplotlib.pyplot as plt


def run_pca(X, n_components, file_name):
    pca = PCA(random_state=0, n_components=n_components)
    pca.fit(X)

    print("== explained_variance_")
    print(pca.explained_variance_)

    print("== explained_variance_ratio_")
    print(pca.explained_variance_ratio_)

    x = range(0, len(pca.explained_variance_ratio_))
    plt.bar(x, pca.explained_variance_ratio_, width=0.4)
    plt.xticks(x)
    plt.title("Distribution of Eigenvalues")
    plt.xlabel("Principal Component")
    plt.ylabel("Explained Variance Ratio")
    plt.savefig("{}_{}.png".format(file_name, datetime.now()), format="png")

    print("== sum explained_variance_ratio_")
    print(np.sum(pca.explained_variance_ratio_))

    print("== principal components")
    print(pd.DataFrame(pca.components_, columns=X.columns))


def run_ica(X, n_components_list, file_name):
    kurtosis_list = []
    for n_components in n_components_list:
        ica = FastICA(
            random_state=0, n_components=n_components, whiten=True, max_iter=2000
        )
        ica.fit(X)
        transformed = ica.transform(X)
        kurtosis = scipy.stats.kurtosis(transformed)
        # kurtosis = scipy.stats.kurtosis(ica.components_)
        kurtosis_list.append(np.average(np.abs(kurtosis)))

    import matplotlib.pyplot as plt

    plt.plot(n_components_list, kurtosis_list)
    plt.grid()
    plt.xticks(n_components_list)
    plt.title("Kurtosis by Number of Components")
    plt.xlabel("Number of Components")
    plt.ylabel("Kurtosis")
    plt.savefig("{}_{}.png".format(file_name, datetime.now()), format="png")

    print(kurtosis_list)


def run_grid_search(
    clf,
    X,
    y,
    param_grid,
    cv=5,
    scoring="f1",
    show_cv_results=False,
    verbose=0,
    n_jobs=-2,
):
    print("\n== Grid Search ==")
    print("\nparam_grid: ")
    print(param_grid)
    grid_search = GridSearchCV(
        clf,
        param_grid,
        cv=cv,
        scoring=scoring,
        return_train_score=True,
        verbose=verbose,
        n_jobs=n_jobs,
    )
    start = time.perf_counter()
    grid_search.fit(X, y)
    end = time.perf_counter()
    print(f"\nGrid search took {end - start:0.4f} seconds")
    if show_cv_results:
        print("\ncv_results_: ")
        print(grid_search.cv_results_)
    return grid_search


def calculate_f1_score(clf, X, y, average="binary"):
    y_predict = clf.predict(X)
    score = f1_score(y, y_predict, average=average)

    # print(confusion_matrix(y, y_predict))
    # print(confusion_matrix(y, y_predict, normalize="true"))
    print("f1_score: {}".format(score))
